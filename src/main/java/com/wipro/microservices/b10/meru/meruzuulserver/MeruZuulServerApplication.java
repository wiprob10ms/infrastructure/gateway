package com.wipro.microservices.b10.meru.meruzuulserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class MeruZuulServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeruZuulServerApplication.class, args);
	}

}
